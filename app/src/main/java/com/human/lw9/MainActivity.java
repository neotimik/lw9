package com.human.lw9;

import android.support.constraint.ConstraintLayout;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.ContextMenu;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {
    TextView textView;
    RelativeLayout relativeLayout;
    int step;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        textView = (TextView) findViewById(R.id.textView);
        textView.setOnCreateContextMenuListener(this);
        relativeLayout = (RelativeLayout) findViewById(R.id.constraintLayout);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.main_menu, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {  // Это выносится в отдельное окно?
        switch (item.getItemId()) {
            case R.id.preferences:
                Toast.makeText(this, "Text size = " + item.getTitleCondensed(), Toast.LENGTH_SHORT).show();
                textView.setTextSize(16);
                return true;
            case R.id.about:
                Toast.makeText(this, "Text size = " + item.getTitleCondensed(), Toast.LENGTH_SHORT).show();
                textView.setTextSize(22);
                return true;
            case R.id.logout:
                Toast.makeText(this, "Text size = " + item.getTitleCondensed(), Toast.LENGTH_SHORT).show();
                textView.setTextSize(28);
                return true;
            default:
                return super.onOptionsItemSelected(item); // Откуда взялось "Ещё" ?
        }
    }

    @Override
    public void onCreateContextMenu(ContextMenu menu, View v, ContextMenu.ContextMenuInfo menuInfo) {
        getMenuInflater().inflate(R.menu.context_menu, menu);
    }

    @Override
    public boolean onContextItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.menu_1:
                Toast.makeText(this, "Increase", Toast.LENGTH_SHORT).show();
                RelativeLayout.LayoutParams lp = new RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.WRAP_CONTENT,RelativeLayout.LayoutParams.WRAP_CONTENT );
                lp = (RelativeLayout.LayoutParams) textView.getLayoutParams();
                lp.setMargins(lp.leftMargin, lp.topMargin + 50, lp.rightMargin, lp.bottomMargin);
               textView.setLayoutParams(lp);
                return true;
            default:
                return super.onContextItemSelected(item);
        }
    }
}
